﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using TouchScript.Behaviors;

public class PuzzleElement : MonoBehaviour {

	[SerializeField]
	public VirtualGrid grid;

	public Transform myTransform;
	public SpriteRenderer spriteRenderer;

	public int positionInGrid;
	public Vector3 cachedPosition;

	void Awake () {
		myTransform = transform;
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	void Start(){
		cachedPosition = myTransform.position;

	}

	private void OnEnable()
	{
		GetComponent<ReleaseGesture>().Released += releaseHandler;
		GetComponent<PressGesture>().Pressed += pressHandler;
	}
	
	private void OnDisable()
	{
		GetComponent<ReleaseGesture>().Released -= releaseHandler;
		GetComponent<PressGesture>().Pressed -= pressHandler;
	}
	
	private void releaseHandler(object sender, System.EventArgs e)
	{
		spriteRenderer.sortingLayerName = "Default";
		OnReleased ();
		Debug.Log ("Released");
	}

	private void pressHandler(object sender, System.EventArgs e)
	{
		spriteRenderer.sortingLayerName = "Pressed";
		Debug.Log ("Pressed");
	}

	private void SetPosiotionOnGrid(Vector3 realPosition){
		grid.SetElement (realPosition, 1, positionInGrid);
		cachedPosition = myTransform.position;
	}

	private void SetRotationOnGrid(Quaternion realRotation){
		myTransform.rotation = grid.GetGridRotation (realRotation, positionInGrid);
	}

	public void OnReleased(){
		SetPosiotionOnGrid (myTransform.position);
		SetRotationOnGrid (myTransform.rotation);
	}

}
