﻿using UnityEngine;
using System.Collections;
using System;


public class VirtualGrid : MonoBehaviour {
	
	[SerializeField]
	GameObject PuzzlePrefab;

	[SerializeField]
	Sprite[] picture;

	[SerializeField]
	EffectsManager effectsManager;

	[SerializeField]
	Data data;

	private int width, height;
	
	public VirtualPuzzleElement[] grid;

	//virtual representation of Puzzles
	[Serializable]
	public struct VirtualPuzzleElement
	{
		public int id;
		public PuzzleElement element;
		public byte rotation;

		public VirtualPuzzleElement(int id, PuzzleElement element, byte rotation){
			this.id = id;
			this.element = element;
			this.rotation = rotation;
		}
	}

	private System.Random rnd = new System.Random();
	
	private void Shuffle<T>(T[] array)
	{
		int n = array.Length;
		for (int i = 0; i < n; i++)
		{
			int r = i + (int)(rnd.NextDouble() * (n - i));
			T t = array[r];
			array[r] = array[i];
			array[i] = t;
		}
	}

	//Util function for "snapping" 
	private float GetDiscreteValue(float value, float space){
		return (int)((int)(value / space)) * space;
	}

	/*private void CopyPuzzleAttributes(VirtualPuzzleElement e1, VirtualPuzzleElement e2){
		e1.id = e2.id;
		e1.rotation = e2.rotation;
		e1.element.positionInGrid = e2.element.positionInGrid;
	}*/

	//updates visuals and virtual grid
	private void SwapPuzzleElements(int index1, int index2){

		int tmpPos = grid [index1].element.positionInGrid;
		Vector3 cachedPos = new Vector3(grid[index1].element.cachedPosition.x, grid[index1].element.cachedPosition.y, grid[index1].element.cachedPosition.z);

		VirtualPuzzleElement tmp = grid [index1];
		grid[index1].element.myTransform.position = grid [index2].element.cachedPosition;
		grid [index1].element.positionInGrid = grid [index2].element.positionInGrid;

		grid [index1].element.cachedPosition = grid [index1].element.myTransform.position;
		grid [index1] = grid [index2];

		grid[index2].element.myTransform.position = cachedPos;
		grid [index2].element.positionInGrid = tmpPos;
		grid [index2].element.cachedPosition = grid [index2].element.myTransform.position;
		grid [index2] = tmp;
	}

	//set grid position for element with given realPosition, replaces two elements
	public void SetElement(Vector3 realPosition, float space, int positionInGrid){
		Vector3 gridPosition = new Vector3 (GetDiscreteValue(realPosition.x, space), GetDiscreteValue(realPosition.y, space), 0);

		int index = (int)gridPosition.x + (int)gridPosition.y*width;

		if (grid [index].id != grid [positionInGrid].id) {
			SwapPuzzleElements (index, positionInGrid);

		} else {
			grid [positionInGrid].element.myTransform.position = gridPosition;
		}

		CheckIfSolved ();
		
	}

	//for "snapping" rotation 
	public Quaternion GetGridRotation(Quaternion realRotation, int positionInGrid){

		float tmp = realRotation.eulerAngles.z;
		Quaternion gridQuaternion;

		if (45 <= tmp && tmp < 135) {
			gridQuaternion = Quaternion.Euler (new Vector3 (0, 0, 90));
		} else if (135 <= tmp && tmp < 225) {
			gridQuaternion = Quaternion.Euler (new Vector3 (0, 0, 180));
		} else if (225 <= tmp && tmp < 315) {
			gridQuaternion = Quaternion.Euler (new Vector3 (0, 0, 270));
		} else {
			gridQuaternion = Quaternion.Euler(new Vector3(0, 0, 0));
		}

		grid [positionInGrid].rotation = (byte)(tmp / 90);

		CheckIfSolved ();

		return gridQuaternion;
	}

	//main logic: if virtual representation follows this conditions then Puzzles are solved
	private void CheckIfSolved(){

		for (int i=0; i<grid.Length; i++) {
			if((i != grid[i].id) || grid[i].rotation != 0){
				return;
			}
		}

		data.Screenshot ();
		effectsManager.OnSolved ();
		Debug.Log ("Solved");
	}

	//Creates visual and virtual grid with given height, width (one slice - 1 Unit) and space (dimensions of slice in Unity Units)
	public void InitializeGrid(int width, int height, float space){

		this.width = width;
		this.height = height;

		grid = new VirtualPuzzleElement[width*height];

		//fill
		for(int i=0; i<grid.Length; i++){

			grid[i].id = i;

			//random rotation
			grid[i].rotation = (byte)(rnd.Next()%4);

			GameObject tmp = (GameObject)Instantiate(PuzzlePrefab, Vector3.zero, Quaternion.Euler(0,0,grid[i].rotation*90));
			tmp.GetComponent<SpriteRenderer>().sprite = picture[i];

			grid[i].element = tmp.GetComponent<PuzzleElement>();
			grid[i].element.grid = this;
 		}

		//permute
		Shuffle (grid);

		//update visuals
		for (int i=0; i<grid.Length; i++) {
			grid[i].element.positionInGrid = i;
			grid[i].element.myTransform.position = new Vector3(space * (i%width), space * ((i/width)%height), 0);
		}


	}


	void Start(){
		//it can be more flexible (needs some kind of menu)
		InitializeGrid (3,3,1);
	}

}
