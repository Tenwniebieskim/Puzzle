﻿using UnityEngine;
using System.Collections;

public class EffectsManager : MonoBehaviour {

	[SerializeField]
	GameObject confetti;

	public void OnSolved(){
		confetti.SetActive (true);
	}

}
