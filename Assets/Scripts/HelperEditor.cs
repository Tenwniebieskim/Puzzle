﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(VirtualGrid))]
public class HelperEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		EditorGUILayout.HelpBox("Adding a photo:\n" +
			"1. Slice the photo into equal pieces\n" +
			"2. Width = # of photos in horizontal axis, etc.\n" +
			"3. Place sliced photos in given pattern (n - # of pieces):\n" +
			"n-1, n, n-3, n-2 etc", MessageType.Info);
		DrawDefaultInspector();
	}
}

#endif